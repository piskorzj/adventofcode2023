const { readFileSync } = require('node:fs')

const find_first_n_last_digit = (entry) => {
    const numbers = entry.match(/([0-9])/g)
    return (numbers.slice(0,1) + numbers.slice(-1))
}

const part1 = (text) => {
    return text.split('\n').map(find_first_n_last_digit).map(str => parseInt(str)).reduce((acc, el) => acc + el, 0)
}

const exampl1 = `1abc2
pqr3stu8vwx
a1b2c3d4e5f
treb7uchet`

console.log('exampl1', part1(exampl1))
console.log('part1', part1(readFileSync('part1input', {'encoding': 'ascii'})))


const reverse = (str) => str.split('').reverse().join('')
const replacepattern = {
    'one': 1,
    'two': 2,
    'three': 3,
    'four':  4,
    'five': 5,
    'six': 6,
    'seven': 7,
    'eight': 8,
    'nine': 9,
}
const re_start = new RegExp('^[^0-9]*?(' + Object.keys(replacepattern).join('|') + ')')
const re_end = new RegExp('^[^0-9]*?(' + reverse(Object.keys(replacepattern).join('|')) + ')')

const part2 = (text) => {
    return text.split('\n').map((entry) => {
        const startfixed = entry.replace(re_start, (match, text) => replacepattern[text])
        const endfixed = reverse(startfixed).replace(re_end, (match, text) => replacepattern[reverse(text)])
        return reverse(endfixed)
    }).map(find_first_n_last_digit).map(str => parseInt(str)).reduce((acc, el) => acc + el, 0)
}

const exampl2 = `two1nine
eightwothree
abcone2threexyz
xtwone3four
4nineeightseven2
zoneight234
7pqrstsixteen`

console.log('exampl2', part2(exampl2))
console.log('part2', part2(readFileSync('part1input', {'encoding': 'ascii'})))
