use std::fs;

fn is_symbol(c: char) -> bool {
    c.is_ascii_punctuation() && c != '.'
}

fn part1(input: &str) -> u32 {
    // algo:
    // iterate over input
    // "normal case":
    // check next element, if number, we are in number phase
    // if this element, or element above, or element below is the symbol set flag
    // advance, check next element and repeat above
    // fi this element is not number, check if symbol, above and below and set flag, end number phase, if flag, add to acumulator

    let mut input_without_nl = input.replace("\n", ".");
    input_without_nl.insert_str(0, ".");

    let input_bytes = input_without_nl.as_bytes();
    let line_len = input
        .lines()
        .next()
        .expect("Should have at least one line...")
        .len()
        + 1; //for nl converted to .;
    let is_in_vertical_line_symbol = |pos: usize| -> bool {
        let mut ret = false;
        if (pos as i64 - line_len as i64) >= 0 {
            ret |= is_symbol(input_bytes[pos - line_len] as char);
        }
        ret |= is_symbol(input_bytes[pos] as char);
        if pos + line_len < input_bytes.len() {
            ret |= is_symbol(input_bytes[pos + line_len] as char);
        }
        ret
    };

    let mut parsing_number = false;
    let mut number: u32 = 0;
    let mut has_symbol = false;

    let mut result = 0;

    for i in 0..input_bytes.len() - 1 {
        if input_bytes[i] as char == '\n' {
            continue;
        }

        if !parsing_number {
            if (input_bytes[i + 1] as char).is_ascii_digit() {
                parsing_number = true
            }
        }

        if parsing_number {
            has_symbol |= is_in_vertical_line_symbol(i);
            if (input_bytes[i + 1] as char).is_ascii_digit() {
                number = number * 10
                    + (input_bytes[i + 1] as char)
                        .to_digit(10)
                        .expect("should be a digit");
            } else {
                //end of numer
                has_symbol |= is_in_vertical_line_symbol(i + 1);

                if has_symbol {
                    result += number;
                }

                has_symbol = false;
                parsing_number = false;
                number = 0;
            }
        }
    }

    result
}

fn find_entire_number(input_bytes: &[u8], pos: usize) -> i32 {
    //find beginig
    let mut beg = pos;
    while beg > 0 && (input_bytes[beg - 1] as char).is_ascii_digit() {
        beg -= 1;
    }
    let mut number: i32 = 0;
    while beg < input_bytes.len() && (input_bytes[beg] as char).is_ascii_digit() {
        number = number * 10
            + (input_bytes[beg] as char)
                .to_digit(10)
                .expect("should be a digit") as i32;
        beg += 1;
    }
    number
}

fn part2(input: &str) -> i32 {
    let mut input_without_nl = input.replace("\n", ".");
    input_without_nl.insert_str(0, ".");
    let input_bytes = input_without_nl.as_bytes();
    let line_len = (input
        .lines()
        .next()
        .expect("Should have at least one line...")
        .len()
        + 1) as i32; //for nl converted to .;

    let find_digits_between_cogs = |pos: usize| -> Option<(i32, i32)> {
        let mut positions: Vec<i32> = Vec::new();
        for y in -1..=1 {
            let mut linepos: Vec<i32> = Vec::new();
            for x in -1..=1 {
                let pos_to_check = pos as i32 + y * line_len + x;
                if pos_to_check >= 0
                    && pos_to_check < input_bytes.len() as i32
                    && (input_bytes[pos_to_check as usize] as char).is_ascii_digit()
                {
                    linepos.push(pos_to_check);
                }
            }
            if linepos.len() == 3 || (linepos.len() == 2 && (linepos[1] - linepos[0]) == 1) {
                linepos.truncate(1);
            }

            positions.append(&mut linepos);
        }

        if positions.len() == 2 {
            Some((
                find_entire_number(input_bytes, positions[0] as usize),
                find_entire_number(input_bytes, positions[1] as usize),
            ))
        } else {
            None
        }
    };

    let mut result = 0;
    for i in 0..input_bytes.len() {
        if input_bytes[i] as char == '*' {
            match find_digits_between_cogs(i) {
                Some((a, b)) => result += a * b,
                None => (),
            }
        }
    }

    result
}

fn main() {
    let exampl1 = r#"467..114..
...*......
..35..633.
......#...
617*......
.....+.58.
..592.....
......755.
...$.*....
.664.598.."#;

    println!("exampl1: {}", part1(&exampl1));
    println!(
        "part1: {}",
        part1(&fs::read_to_string("part1input").expect("Unable to read file"))
    );

    println!("exampl2: {}", part2(&exampl1));
    println!(
        "part2: {}",
        part2(&fs::read_to_string("part1input").expect("Unable to read file"))
    );
}
