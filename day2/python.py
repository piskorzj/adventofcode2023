import re
import math

#"""12 red cubes, 13 green cubes, and 14 blue cubes"""
criteria = {
    'red': 12,
    'green': 13,
    'blue': 14,
}

re_game_info = re.compile("(\d+) (green|blue|red)")
def part1_parseline(line):
    """ returns number, game number if matches criteria """
    game_text, data_text = line.split(":")
    game = int(game_text[5:])
    for data in data_text.split(";"):
        for (count, colour) in re_game_info.findall(data):
            if int(count) > criteria[colour]:
                return 0
    return game

def part1(text):
    return sum([part1_parseline(line) for line in text.split("\n")])

exampl1 = """Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green
Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue
Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red
Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red
Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green"""

print("exampl1", part1(exampl1))
with open('part1input', encoding="ascii") as f:
    print("part1", part1(f.read()))

def create_or_append(dic, key, value):
    if key in dic:
        dic[key].append(value)
    else:
        dic[key] = [value]


def part2_parseline(line):
    """ returns number, game number if matches criteria """
    game_text, data_text = line.split(":")
    colors = {}
    for data in data_text.split(";"):
        for (count, colour) in re_game_info.findall(data):
            create_or_append(colors, colour, int(count))
    return math.prod([ max(one_color) for one_color in colors.values()])

def part2(text):
    return sum([part2_parseline(line) for line in text.split("\n")])

print("exampl2", part2(exampl1))
with open('part1input', encoding="ascii") as f:
    print("part2", part2(f.read()))
